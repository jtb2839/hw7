CC:=gcc

hw7_secant: hw7_secant.o
	$(CC) -o $@ $^ -lm
hw7_newton1: hw7_newton1.o
	$(CC) -o $@ $^ -lm
hw7_newton2: hw7_newton2.o
	$(CC) -o $@ $^ -lm
hw7_secant.o: hw7_secant.c
	$(CC) -c $<
hw7_newton1.o: hw7_newton1.c
	$(CC) -c $<
hw7_newton2.o: hw7_newton2.c
	$(CC) -c $<

run_secant:hw7_secant
	./hw7_secant 0 100 .000001 20
run_n1:hw7_newton1
	./hw7_newton1 100 .000001 20
run_n2_1:hw7_newton2
	./hw7_newton2 .5 .000001 20
run_n2_2:hw7_newton2
	./hw7_newton2 .75 .000001 20
all: hw7_newton1 hw7_newton2 hw7_secant
