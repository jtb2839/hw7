#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

double f(double x)
{
	return x/(1+x*x);
}
double fp(double x)
{
	return 1/(x*x+1)-2*x*x/(1+x*x)/(1+x*x);
}
int main(int argc, char* argv[])
{
	double curr=atof(argv[1]);
	double eps=atof(argv[2]);
	int maxIter=atoi(argv[3]);
	double currEps;
	int i;
	for(i=0;i<maxIter;i++)
	{
		curr=curr-f(curr)/fp(curr);
		currEps=fabs(curr-curr/(1+curr*curr));
		printf("Iteration %d: x=%f, error=%4.10f\n",i+1,curr,currEps);
		if(currEps<eps)
			break;
	}
}
