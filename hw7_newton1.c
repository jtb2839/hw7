#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

double f(double x)
{
	return 31-x*x;
}
double fp(double x)
{
	return -2*x;
}
int main(int argc, char *argv[])
{
	double curr=atof(argv[1]);
	double eps=atof(argv[2]);
	int maxIter=atoi(argv[3]);
	double currEps;
	int i;
	for(i=0;i<maxIter;i++)
	{
		curr=curr-f(curr)/fp(curr);
		currEps=fabs(31-curr*curr);
		printf("Iteration %d: x= %2.10f, error= %4.10f\n",i+1,curr,currEps);
		if(currEps<eps)
			break;
	}
}
