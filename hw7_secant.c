#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
	double last=atof(argv[1]);
	double curr=atof(argv[2]);
	double eps=atof(argv[3]);
	int maxIter=atoi(argv[4]);
	//last,curr,eps,maxIter
	double answer=24;
	int i;
	int num=576;
	
	double next;
	//printf("%f\t%f\t%f\t%d\n",last,curr,eps,maxIter);
	for(i=0;i<maxIter;i++)
	{
		next=curr-(curr*curr-num)*((curr-last)/(curr*curr-last*last));
		last=curr;
		curr=next;
		double currEps=fabs((answer*answer-curr*curr));
		printf("Iteration %d: x=%f, error=%3.10f\n",i+2,curr,currEps);
		if(currEps<eps)
		{
			break;
		}
	}
}
